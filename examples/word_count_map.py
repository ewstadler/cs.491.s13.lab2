#!/usr/bin/env python2

import sys

for line in sys.stdin:
    key, value = line.split('\t', 1)
    for word in value.strip().split():
        print '%s\t%d' % (word, 1)

# vim: set sts=4 sw=4 ts=8 expandtab ft=python:
