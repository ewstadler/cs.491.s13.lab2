#!/usr/bin/env python2

import sys

key   = None
total = ''
for line in sys.stdin:
    k, v  = line.split('\t', 1)
    count = v.rstrip()

    if key == k:
        total += count.rstrip() + " "
    else:
        if key:
            print '%s\t1.0 %s' % (key.rstrip(), total.rstrip())
        key   = k
        total = count.rstrip()

if key:
    print '%s\t1.0 %s' % (key.rstrip(), total.rstrip())

# vim: set sts=4 sw=4 ts=8 expandtab ft=python:
