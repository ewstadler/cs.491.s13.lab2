#!/usr/bin/env python2

import sys
import re
import os

for line in sys.stdin:
	key, value = line.split('\t', 1)
	filepath, linenumber = line.split(':', 1)
	filename = filepath.split('/')
	pattern = re.search("\w+ \w+\(.*\)",line)
	if (pattern):
		name = re.search("\s[\w]+\(",pattern.group())
		print '%s\t%s' % (name.group().strip('\('), filename[-1].rstrip())