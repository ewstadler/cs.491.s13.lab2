#!/usr/bin/env python2

import collections
import sys

data = collections.defaultdict(int)
for line in sys.stdin:
    k, v = line.split('\t', 1)
    data[k] += int(v.strip())

for k in sorted(data.keys()):
    print('{0}\t{1}'.format(k, data[k]))

# vim: set sts=4 sw=4 ts=8 expandtab ft=python:
