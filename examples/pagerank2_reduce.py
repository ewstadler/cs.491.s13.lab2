#!/usr/bin/env python2

import sys

key   = None
total = ''
for line in sys.stdin:
	#print 'aa %s' % (line)
	k, v = line.split('\t', 1)
	#count = v.rstrip()
	#splitValue = count.split()
	outlink,pagerank,number = v.split(" ",2)
	number = float(number)
	pagerank = float(pagerank)
	
	if key == k:
		total += outlink.rstrip() + " "
		pagerank = (0.85 * pagerank/number) + (1 - 0.85)
	else:
		if key:
			print '%s\t%f %s' % (key, pagerank, total)
		key   = k
		total = outlink

if key:
    print '%s\t%d %s' % (key, pagerank, total)

# vim: set sts=4 sw=4 ts=8 expandtab ft=python:
