#!/usr/bin/env python2

import sys
import re
import os

for line in sys.stdin:
	key, value = line.split('\t', 1)
	filepath, linenumber = line.split(':', 1)
	filename = filepath.split('/')
	for word in line.split():
		if (re.search("http://[\w]+",word)):
			print '%s\t%s' % (word, filename[-1].rstrip())
