#!/usr/bin/env python2

import sys

key   = None
total = 0
for line in sys.stdin:
    k, v  = line.split('\t', 1)
    count = int(v.strip())

    if key == k:
        total += count
    else:
        if key:
            print '%s\t%d' % (key, total)
        key   = k
        total = count

if key:
    print '%s\t%d' % (key, total)

# vim: set sts=4 sw=4 ts=8 expandtab ft=python:
