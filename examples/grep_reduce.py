#!/usr/bin/env python2

import sys

key   = None
total = ''
for line in sys.stdin:
    k, v  = line.split('\t', 1)
    count = v

    if key == k:
        total += count.rstrip() + " "
    else:
        if key:
            print '%s\t%s' % (key, total)
        key   = k
        total = count

if key:
    print '%s\t%s' % (key, total)

# vim: set sts=4 sw=4 ts=8 expandtab ft=python:
