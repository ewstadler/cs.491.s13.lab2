#!/usr/bin/env python2

import sys
import re
import os

for line in sys.stdin:
	filepath, linenumber = line.split(':', 1)
	filename = filepath.split('/')
	pattern = re.search("#include [<\"].+",line)
	if (pattern):
		name = re.search("[<\"].+[>\"]",pattern.group())
		shortname = name.group().strip('<>\"').split('/')
		print '%s\t%s' % (filename[-1].rstrip(), shortname[-1].rstrip())