WorkQueue MapReduce
===================

This is a simple implementation of [MapReduce] using [Python] and [WorkQueue]
that supports the [Hadoop Streaming] interface.

Requirements
------------

- [Python] 2.4 - 2.7

    The master and workers require [Python] to execute.

- [CCTools] 3.6+

    The master requires the Python-WorkQueue bindings to coordinate the tasks,
    while the workers are provided by `work_queue_worker`.

Usage
-----

    Usage: work_queue_mapreduce.py [options]

    Options:
      -h, --help                              show this help message and exit

    MapReduce Options:
      -i INPUT_FILE, --input_file=INPUT_FILE  path to list of inputs
      -m MAPPER, --mapper=MAPPER              path to Map executable
      -M NMAPPERS, --nmappers=NMAPPERS        number of Mappers
      -r REDUCER, --reducer=REDUCER           path to Reduce executable
      -R NREDUCERS, --nreducers=NREDUCERS     number of Reducers
      -c COMBINER, --combiner=COMBINER        path to Combiner executable
      -w WORK_DIR, --work_dir=WORK_DIR        path to working directory
      -S, --enable_status                     enable status monitor

    WorkQueue Options:
      -N NAME, --name=NAME                    name for WorkQueue project
      -p PORT, --port=PORT                    port for WorkQueue master

    Logging Options:
      -l LOG_PATH, --log_path=LOG_PATH        path to log file
      -v, --verbose                           enable verbose logging

   Internal Options:
      -I ID, --id=ID                          internal MapReduce identifier
      -P PHASE, --phase=PHASE                 internal MapReduce phase

Word Count Example
------------------

The WorkQueue MapReduce distribution provides an example of the canonical *word
count* MapReduce application in the `examples` directory.  To execute this
example, do the following:

### Word Count WorkQueueMapReduce Source

Start the WorkQueue MapReduce master:

    $ ./work_queue_mapreduce.py -m examples/word_count_map.py -r examples/word_count_reduce.py examples/*.py *.py &

Start a WorkQueue worker:

    $ work_queue_worker -d all localhost 9123

These commands will start a WorkQueue MapReduce master that will attempt to
perform word count on all of the Python source code in the current directory
and then a local WorkQueue worker that will do the actual computation.

### Word Count Linux Kernel Source

For an example of word counting a larger corpus of data, we can perform a
MapReduce on the [Linux] source code:

Fetch and extract the Linux source code:

    $ wget http://www.kernel.org/pub/linux/kernel/v3.0/linux-3.8.tar.bz2
    $ tar xf linux-3.8.tar.bz2

Construct a list of input files:

    $ find linux-3.8 -iname '*.[ch]' > inputs

Start the WorkQueue MapReduce master:

    $ ./work_queue_mapreduce.py -m examples/word_count_map.py -M 100 -r examples/word_count_reduce.py -R 1 -i inputs &

Start a WorkQueue worker:

    $ work_queue_worker -d all localhost 9123

If you wish to run the Word Count example locally without WorkQueue, you can do
the following:

    $ ./work_queue_mapreduce.py -P Inputs -I 0 -i inputs | ./examples/word_count_map.py | sort | ./examples/word_count_reduce.py > outputs

Hadoop Streaming
----------------

To write a [MapReduce] application using WorkQueue MapReduce, users simply
provide a set of input files, a **mapper**, and a **reducer**.

Here is the high-level data flow:

    Inputs -> [Mappers] -> Key-Values -> [Reducers] -> Outputs

### Inputs

The **inputs** to a typical WorkQueue MapReduce application are a set of text
files.  These will be sent to remote workers and then streamed into your
**mapper** via `standard input`.  The **inputs** will be in the form of
`key\tvalue` pairs where the `key` is `filename:linenumber` while the
`value` is the actual contents of the line.

### Mapper

The **mapper** reads the input data stream via `standard input` and emits
intermediate *key/value* pairs in the form of `key\tvalue` (ie. `key`, `tab
character`, `value`) to `standard output`.  These *key/value* pairs be
partitioned by the **worker** while executing the **mapper** function and the
sent back to the master.

### Reducer

The master will shuffle the intermediate *key/value* pairs and send them to the
appropriate **reducers**.  Once at the remote node, the **reducers** will sort
the data and then apply the **reducer** function to the stream of sorted
*key/value* pairs (again via `standard input`).  The **reducer** function
should then emit another stream of *key/value* pairs to `standard out`.

### Outputs

The results of MapReduce will be stored in the *work directory*.  Unless
specified, this is `wqmr-$USER-$PID`.

In summary, both the **mapper** and **reducer** functions are executables (e.g.
scripts) that read from  `standard input` and emit *key/value* pairs to
*standard output*.

Work Queue
----------

Please consult the [WorkQueue User's Manual] and the [CCTools Manpages] for
more information about WorkQueue.

[Linux]:	    http://kernel.org/
[MapReduce]:	    http://research.google.com/archive/mapreduce.html
[Python]:	    http://www.python.org/
[WorkQueue]:	    http://www.cse.nd.edu/~ccl/software/workqueue/
[Hadoop Streaming]: http://hadoop.apache.org/docs/stable/streaming.html
[CCTools]:	    http://www3.nd.edu/~ccl/software/
[CCTools Manpages]: http://www3.nd.edu/~ccl/software/manuals/
[WorkQueue User's Manual]:  http://www3.nd.edu/~ccl/software/manuals/workqueue.html
