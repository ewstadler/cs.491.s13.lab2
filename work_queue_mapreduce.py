#!/usr/bin/env python2

import collections
import datetime
import itertools
import logging
import math
import optparse
import os
import subprocess
import sys
import time

try:
    import work_queue   # Only require for Master
except ImportError:
    # TODO: handle this gracefully
    pass

#-------------------------------------------------------------------------------
# Defaults
#-------------------------------------------------------------------------------

MAX_MAP_INPUTS = 512

#-------------------------------------------------------------------------------
# WorkQueue Tasks Stats Class
#-------------------------------------------------------------------------------

class WorkQueueTasksStats(object):
    def __init__(self, submitted=None, succeeded=None, failed=None):
        self.submitted = submitted or 0
        self.succeeded = succeeded or 0
        self.failed    = failed    or 0

    @property
    def progress(self):
        if self.submitted:
            progress = '='*(self.succeeded * 50/ self.submitted)
        else:
            progress = ''

        return '[%-50s]' % (progress)

    @property
    def percent(self):
        if self.submitted:
            percent = self.succeeded*100.0/self.submitted
        else:
            percent = 0

        return '%6.2f%%' % (percent)

#-------------------------------------------------------------------------------
# WorkQueue MapReduce Class
#-------------------------------------------------------------------------------

class WorkQueueMapReduce(object):
    NMAPPERS    = 4
    NREDUCERS   = 1
    STATES      = {
        'Map'          : (lambda s: s.run_map()   , 'Reduce'),
        'Reduce'       : (lambda s: s.run_reduce(), 'Finish'),
        'Finish'       : (None, None),    # TODO: Merge?
    }

    def __init__(self, mapper, reducer, inputs,
        combiner=None, nmappers=None, nreducers=None,
        port=None, name=None, catalog=True, exclusive=True, shutdown=False,
        log_level=None, log_path=None, work_dir=None, enable_status=False):
        self.state         = 'Map'
        self.mapper        = mapper
        self.reducer       = reducer
        self.inputs        = inputs
        self.combiner      = combiner
        self.logger        = make_logger('WorkQueueMapReduce', log_level, log_path, enable_status)
        self.nmappers      = nmappers  or WorkQueueMapReduce.NMAPPERS
        self.nreducers     = nreducers or WorkQueueMapReduce.NREDUCERS
        self.work_queue    = work_queue.WorkQueue(
            port      = port or 0,
            name      = name or 'wqmr-%s-%05d' % (os.environ['USER'], os.getpid()),
            catalog   = True,
            exclusive = exclusive,
            shutdown  = shutdown)
        self.work_dir      = work_dir or os.path.join(os.curdir, self.work_queue.name)
        self.map_tasks     = WorkQueueTasksStats(0, 0, 0)
        self.reduce_tasks  = WorkQueueTasksStats(0, 0, 0)
        self.start_time    = time.time()
        self.work_time     = None
        self.enable_status = enable_status

        if not os.path.exists(self.work_dir):
            os.makedirs(self.work_dir)

        self.logger.info('Port             : %d' % (self.work_queue.port))
        self.logger.info('Working Directory: %s' % (self.work_dir))

    def run_map(self):
        ''' Schedule Mapper tasks '''
        chunks = int(math.ceil(len(self.inputs)/float(self.nmappers)) or 1)
        splits = groups(self.inputs, chunks)

        for mapper_id, input_paths in enumerate(splits):
            # Ensure that we don't have too many input file arguments
            # If it is too large, then the worker will segfault and cause
            # the master to segfault.
            if len(input_paths) > MAX_MAP_INPUTS:
                self.logger.critical(
                    'Number of inputs (%d) for Map Tasks is too high!' % (len(input_paths)))
                self.logger.critical(
                    'Consider a larger value for NMappers (%d)!' % (self.nmappers))
                sys.exit(1)

            local_paths  = [__file__, self.mapper] + input_paths
            remote_paths = [os.path.basename(f) for f in local_paths]

            if self.combiner:
                combiner_name = os.path.basename(self.combiner)
                local_paths.insert(2, self.combiner)
                remote_paths.insert(2, combiner_name)
                combiner = '-c %s' % combiner_name
            else:
                combiner = ''

            # Store true input paths to a text file.  This is useful because it
            # allows the mapper to know true paths of the input files.
            try:
                inputs_name = 'm%04d.inputs' % (mapper_id)
                inputs_path = os.path.join(self.work_dir, inputs_name)
                inputs_file = open(inputs_path, 'w+')
                for input_path in input_paths:
                    inputs_file.write(input_path + '\n')
            finally:
                inputs_file.close()

            task = work_queue.Task('python %s -P Map -m %s -I %d -R %d %s -i %s' % (
                remote_paths[0],
                remote_paths[1],
                mapper_id,
                self.nreducers,
                combiner,
                inputs_name))

            task.specify_input_file(inputs_path, inputs_name)

            for local_path, remote_path in zip(local_paths, remote_paths):
                task.specify_input_file(local_path, remote_path)

            for reducer_id in range(self.nreducers):
                partition_path = 'm%04d_r%04d.out' % (mapper_id, reducer_id)
                task.specify_output_file(os.path.join(self.work_dir, partition_path), partition_path)

            task.specify_tag('m%04d' % (mapper_id))
            self.work_queue.submit(task)
            self.map_tasks.submitted += 1

            self.logger.info('Task %s submitted with command:\n\t%s' % (task.tag, task.command))

    def run_reduce(self):
        ''' Schedule Reducer tasks '''
        for reducer_id in range(self.nreducers):
            partition_paths = []

            for mapper_id in range(self.nmappers):
                partition_path = 'm%04d_r%04d.out' % (mapper_id, reducer_id)
                partition_path = os.path.join(self.work_dir, partition_path)
                if os.path.exists(partition_path):
                    partition_paths.append(partition_path)

            local_paths  = [__file__, self.reducer] + partition_paths
            remote_paths = [os.path.basename(f) for f in local_paths]

            task = work_queue.Task('python %s -P Reduce -r %s -I %d %s' % (
                remote_paths[0],
                remote_paths[1],
                reducer_id,
                ' '.join(remote_paths[2:])))

            for local_path, remote_path in zip(local_paths, remote_paths):
                task.specify_input_file(local_path, remote_path)

            output_path = 'r%04d.out' % (reducer_id)
            task.specify_output_file(os.path.join(self.work_dir, output_path), output_path)
            task.specify_tag('r%04d' % (reducer_id))
            self.work_queue.submit(task)
            self.reduce_tasks.submitted += 1

            self.logger.info('Task %s submitted with command:\n\t%s' % (task.tag, task.command))

    def display_status(self, event):
        wq_stats = self.work_queue.stats
        workers_stats = 'I: %8d R: %8d B: %8d J: %8d Q: %8d' % (
            wq_stats.workers_init,
            wq_stats.workers_ready,
            wq_stats.workers_busy,
            wq_stats.total_workers_joined,
            wq_stats.total_workers_removed)
        tasks_stats = 'W: %8d R: %8d U: %8d D: %8d C: %8d' % (
            wq_stats.tasks_waiting,
            wq_stats.tasks_running,
            wq_stats.tasks_complete,
            wq_stats.total_tasks_dispatched,
            wq_stats.total_tasks_complete)
        data_stats = 'S: %8s R: %8s' % (
            bytes_format(wq_stats.total_bytes_sent),
            bytes_format(wq_stats.total_bytes_received))

        if self.work_time is None:
            work_time_start = 'N/A'
            work_time_diff  = 0
        else:
            work_time_start = datetime.datetime.fromtimestamp(self.work_time).ctime()
            work_time_diff  = time.time() - self.work_time

        kwargs = dict(
            name                = self.work_queue.name,
            port                = self.work_queue.port,
            work_dir            = self.work_dir,
            map_progress        = self.map_tasks.progress,
            map_percent         = self.map_tasks.percent,
            reduce_progress     = self.reduce_tasks.progress,
            reduce_percent      = self.reduce_tasks.percent,
            workers_stats       = workers_stats,
            tasks_stats         = tasks_stats,
            data_stats          = data_stats,
            start_time          = datetime.datetime.fromtimestamp(self.start_time).ctime(),
            work_time           = work_time_start,
            elapsed_time        = elapsed_time_format(time.time() - self.start_time),
            elapsed_work_time   = elapsed_time_format(work_time_diff),
            event               = event)
        print '''WorkQueue MapReduce
================================================================================

             Name: %(name)s
             Port: %(port)s
   Work Directory: %(work_dir)s

              Map: %(map_progress)s %(map_percent)s
           Reduce: %(reduce_progress)s %(reduce_percent)s

          Workers: %(workers_stats)s
            Tasks: %(tasks_stats)s
             Data: %(data_stats)s

       Start Time: %(start_time)s
        Work Time: %(work_time)s
     Elapsed Time: %(elapsed_time)s
Elapsed Work Time: %(elapsed_work_time)s

       Last Event: %(event)s
''' % kwargs

    def run(self):
        ''' Execute MapReduce workflow '''

        # Loop until we reach Finish state
        event = None
        last_clear = time.time()

        while self.state != 'Finish' and self.map_tasks.failed == 0:

            # Retrieve state function and execute current state
            function, next_state = WorkQueueMapReduce.STATES[self.state]
            function(self)

            # Wait for all state tasks to complete
            while not self.work_queue.empty():
                wq_stats   = self.work_queue.stats
                wq_workers = wq_stats.workers_init + \
                             wq_stats.workers_ready + \
                             wq_stats.workers_busy
                if self.work_time is None and wq_workers > 0:
                    self.work_time = time.time()

                task = self.work_queue.wait(1)
                if task:
                    result = task.result or task.return_status
                    output = task.output or ''
                    tag    = task.tag
                    event  = 'Task %s returned with exit status %s' % (tag, result)


                    if tag.startswith('m'):
                        stats = self.map_tasks
                    else:
                        stats = self.reduce_tasks

                    if result:
                        output = '\n' + output
                        stats.failed += 1
                    else:
                        stats.succeeded += 1

                    self.logger.info('%s%s' % (event, output))

                    # Record task output to log file
                    log_path = os.path.join(self.work_dir, tag + '.log')
                    log_file = open(log_path, 'w+')

                    try:
                        log_file.write(task.output)
                    finally:
                        log_file.close()

                if self.enable_status and time.time() - last_clear >= 1.0:
                    os.system('clear')
                    self.display_status(event)
                    last_clear = time.time()

            # Transition to next state
            self.state = next_state

        if self.enable_status:
            os.system('clear')

        self.display_status(event)

        # Return 0 only if we have no failed tasks
        sys.exit(self.map_tasks.failed + self.reduce_tasks.failed)

#-------------------------------------------------------------------------------
# Worker Functions
#-------------------------------------------------------------------------------

def worker_inputs(mapper_id, inputs):
    os.environ['WQMR_MAPPER_ID'] = str(mapper_id)

    # For each input file, try to open basename first since WQ will transfer a
    # local copy of the file to the remote worker.  Otherwise, use the full
    # path when trying to open the file.  Always use the full path as the key,
    # however, so that the mapper knows the true path of the file.
    for input_path in inputs:
        input_name = os.path.basename(input_path)
        if os.path.exists(input_name):
            data = open(input_name)
        else:
            data = open(input_path)

        for i, value in enumerate(data):
            key = '%s:%s' % (input_path, i)
            sys.stdout.write('%s\t%s' % (key, value))

        data.close()

def worker_map(mapper, mapper_id, nreducers, inputs, combiner=None):
    ''' Map, Partition, and Sort '''

    # Map inputs
    inputs_process = subprocess.Popen(
        [os.path.abspath(__file__), '-P', 'Inputs', '-I', str(mapper_id)] + inputs,
        stdin  = None,
        stdout = subprocess.PIPE)
    mapper_process = subprocess.Popen(
        [os.path.abspath(mapper)],
        stdin  = inputs_process.stdout,
        stdout = subprocess.PIPE)

    # Partition data
    partition_paths = ['m%04d_r%04d.tmp' % (mapper_id, reducer_id)
                       for reducer_id in range(nreducers)]
    partition_files = [open(f, 'w+') for f in partition_paths]
    for line in mapper_process.stdout:
        try:
            key, value = line.split('\t', 1)
            partition  = hash(key) % nreducers
            partition_files[partition].write('%s\t%s' % (key, value))
        except ValueError:
            pass

    for process in [inputs_process, mapper_process]:
        result = process.wait()
        if result != 0:
            sys.exit(result)

    for partition_file in partition_files:
        partition_file.close()

    # Sort and combine data
    environ = os.environ.copy()
    environ['LC_ALL'] = 'C'
    for reducer_id in range(nreducers):
        input_path     = 'm%04d_r%04d.tmp' % (mapper_id, reducer_id)
        output_path    = 'm%04d_r%04d.out' % (mapper_id, reducer_id)
        sorter_command = [find_executable('sort'), '-s', input_path]

        if combiner:
            sorter_output = subprocess.PIPE
        else:
            sorter_output = open(output_path, 'w+')

        sorter_process = subprocess.Popen(
            sorter_command,
            stdin  = None,
            stdout = sorter_output,
            env    = environ)

        processes = [sorter_process]

        if combiner:
            combiner_process = subprocess.Popen(
                [os.path.abspath(combiner)],
                stdin  = sorter_process.stdout,
                stdout = open(output_path, 'w+'))

            processes.append(combiner_process)

        for process in processes:
            result = process.wait()
            if result != 0:
                sys.exit(result)


def worker_reduce(reducer, reducer_id, inputs):
    ''' Merge and Reduce '''

    # Use C/POSIX locale
    environ = os.environ.copy()
    environ['LC_ALL'] = 'C'

    # Merge partitions
    output_path = 'r%04d.out' % (reducer_id)
    merge_process  = subprocess.Popen([find_executable('sort'), '-m'] + inputs,
        stdin  = None,
        stdout = subprocess.PIPE,
        env    = environ)

    # Reduce partition
    reduce_process = subprocess.Popen([os.path.abspath(reducer)],
        stdin  = merge_process.stdout,
        stdout = open(output_path, 'w+'))

    # Wait for processes to end
    for process in [merge_process, reduce_process]:
        result = process.wait()
        if result != 0:
            sys.exit(result)


#-------------------------------------------------------------------------------
# Utility Functions
#-------------------------------------------------------------------------------

def find_executable(executable, find_dirs=None):
    ''' Return absolute path of executable

    This searches the current directory and the paths in the $PATH environment
    variable.
    '''
    if os.path.exists(executable):
        return os.path.abspath(executable)

    if find_dirs is None:
        find_dirs = []

    find_dirs = itertools.chain(find_dirs, [os.curdir], os.environ['PATH'].split(':'))

    for dir_path in find_dirs:
        exe_path = os.path.join(dir_path, executable)
        if os.path.exists(exe_path):
            return exe_path

    return None

def groups(data, n):
    ''' Return n-sized sub-lists '''
    return [data[i:i+n] for i in range(0, len(data), n)]

def elapsed_time_format(seconds):
    ''' Return elapsed time formatted as year:day:hour:minute:second '''
    tdeltas = (60, 60, 24, 365, 10)
    tlist   = []
    ctime   = int(seconds)

    if seconds is None:
        return None

    for d in tdeltas:
        if ctime >= d:
            tlist.append(ctime % d)
            ctime = ctime / d
        else:
            tlist.append(ctime)
            break

    return ':'.join(reversed(['%02d' % (t) for t in tlist]))

def bytes_format(bytes):
    ''' Return bytes in human-readable form '''
    K, M, G, T = 1 << 10, 1 << 20, 1 << 30, 1 << 40

    if   bytes >= T:
        result = '%6.2fTB' % (float(bytes) / T)
    elif bytes >= G:
        result = '%6.2fGB' % (float(bytes) / G)
    elif bytes >= M:
        result = '%6.2fMB' % (float(bytes) / M)
    elif bytes >= K:
        result = '%6.2fKB' % (float(bytes) / K)
    else:
        result = '%6d B' % (bytes)

    return result

#-------------------------------------------------------------------------------
# Logger Factory Function
#-------------------------------------------------------------------------------

def make_logger(name, level=None, path=None, enable_status=False):
    ''' Returns a configured logger '''
    level     = level or logging.INFO
    logger    = logging.getLogger(name)
    formatter = logging.Formatter(
        fmt     = '%(asctime)s.00 [' + str(os.getpid()) + '] %(name)s: %(message)s',
        datefmt = '%Y/%m/%d %H:%M:%S')
    handlers  = []

    if not enable_status:
        handlers.append(logging.StreamHandler())
    else:
        handler = logging.StreamHandler()
        handler.setLevel(logging.CRITICAL)
        handler.setFormatter(formatter)
        logger.addHandler(handler)

    if path:
        handlers.append(logging.FileHandler(path))

    for handler in handlers:
        handler.setLevel(level)
        handler.setFormatter(formatter)
        logger.addHandler(handler)

    logger.setLevel(level)
    return logger

#-------------------------------------------------------------------------------
# Executable Script Wrapper Function
#-------------------------------------------------------------------------------

def work_queue_mapreduce():
    ''' Parse command line arguments and execute WorkQueueMapReduce '''

    # Setup option parser
    formatter = optparse.IndentedHelpFormatter(indent_increment=2, max_help_position=44, width=79)
    parser    = optparse.OptionParser(formatter=formatter)

    general = parser.add_option_group('MapReduce Options')
    general.add_option('-i', '--input_file', action='store',
        help='path to list of inputs')
    general.add_option('-m', '--mapper', action='store',
        help='path to Map executable')
    general.add_option('-M', '--nmappers', action='store',
        help='number of Mappers', type=int, default=WorkQueueMapReduce.NMAPPERS)
    general.add_option('-r', '--reducer', action='store',
        help='path to Reduce executable')
    general.add_option('-R', '--nreducers', action='store',
        help='number of Reducers', type=int, default=WorkQueueMapReduce.NREDUCERS)
    general.add_option('-c', '--combiner', action='store',
        help='path to Combiner executable')
    general.add_option('-w', '--work_dir', action='store',
        help='path to working directory', default=None)
    general.add_option('-S', '--enable_status', action='store_true',
        help='enable status monitor', default=False)

    # TODO: add more Work Queue options
    workqueue = parser.add_option_group('WorkQueue Options')
    workqueue.add_option('-N', '--name', action='store',
        help='name for WorkQueue project', default=None)
    workqueue.add_option('-p', '--port', action='store',
        help='port for WorkQueue master', default=None, type=int)

    logger = parser.add_option_group('Logging Options')
    logger.add_option('-l', '--log_path', action='store',
        help='path to log file', default=None)
    logger.add_option('-v', '--verbose', action='store_true',
        help='enable verbose logging', default=False)

    internal = parser.add_option_group('Internal Options')
    internal.add_option('-I', '--id', action='store',
        help='internal MapReduce identifier', type=int)
    internal.add_option('-P', '--phase', action='store',
        help='internal MapReduce phase', default=None)

    # Parse arguments and dispatch based on MapReduce phase
    args, inputs = parser.parse_args()

    args.inputs = inputs
    if args.input_file:
        args.inputs = [input_file.strip() for input_file in open(args.input_file)]

    if not args.inputs:
        parser.print_help()
        sys.exit(1)

    log_level = None
    if args.verbose:
        log_level = logging.DEBUG

    # TODO: handle KeyboardInterrupt gracefully
    if args.phase is None:
        if args.log_path:
            work_queue.cctools_debug_config_file(args.log_path)
            work_queue.set_debug_flag('all')

        map_reduce = WorkQueueMapReduce(
            mapper        = args.mapper,
            reducer       = args.reducer,
            inputs        = args.inputs,
            combiner      = args.combiner,
            nmappers      = args.nmappers,
            nreducers     = args.nreducers,
            name          = args.name,
            port          = args.port,
            log_level     = log_level,
            log_path      = args.log_path,
            work_dir      = args.work_dir,
            enable_status = args.enable_status)

        if not map_reduce.mapper:
            map_reduce.logger.critical('Mapper not specified')
            sys.exit(1)

        if not os.path.exists(map_reduce.mapper):
            map_reduce.logger.critical('Mapper does not exist: %s' % map_reduce.mapper)
            sys.exit(1)

        if not map_reduce.reducer:
            map_reduce.logger.critical('Reducer not specified')
            sys.exit(1)

        if not os.path.exists(map_reduce.reducer):
            map_reduce.logger.critical('Reducer does not exist: %s' % map_reduce.reducer)
            sys.exit(1)

        if map_reduce.combiner and not os.path.exists(map_reduce.combiner):
            map_reduce.logger.critical('Combiner does not exist: %s' % map_reduce.combiner)
            sys.exit(1)

        map_reduce.run()
    elif args.phase.lower() == 'inputs':
        worker_inputs(args.id, args.inputs)
    elif args.phase.lower() == 'map':
        worker_map(args.mapper, args.id, args.nreducers, args.inputs, args.combiner)
    elif args.phase.lower() == 'reduce':
        worker_reduce(args.reducer, args.id, args.inputs)
    else:
        parser.print_help()
        sys.exit(1)

#-------------------------------------------------------------------------------
# Main Execution
#-------------------------------------------------------------------------------

if __name__ == '__main__':
    work_queue_mapreduce()

# vim: set sts=4 sw=4 ts=8 expandtab ft=python:
